jest.mock('../publisher');
const server = require('../server');
const request = require('supertest');
const dbHelper = require('./db-test-helper')();
const { productDeleted } = require('../publisher');

jest.mock('../uploader', () => {
  return {
    productImages: {
      remove: jest.fn(() => Promise.resolve())
    }
  };
});

const uploader = require('../uploader');

const productCategories = [
  {
    name: 'A'
  },
  {
    name: 'B'
  },
  {
    name: 'C'
  }
];

describe('DELETE /api/products', () => {
  beforeAll(async () => {
    await dbHelper.setupDb();
    await dbHelper.resetDb();
    await dbHelper.seedDb({ productCategories });
  });
  beforeEach(async () => {
    await dbHelper.resetDb(['productImages', 'products']);
    uploader.productImages.remove.mockClear();
    productDeleted.mockClear();
  });
  afterAll(async () => {
    await dbHelper.dropDb();
  });
  it('deletes a product', async () => {
    const product = {
      name: 'Super Product',
      price: 99.99,
      categoryId: 1
    };

    await dbHelper.seedDb({ products: [product] });

    const response = await request(server).delete('/api/products/1');

    expect(response.status).toBe(204);
    expect(productDeleted).toHaveBeenCalledTimes(1);
  });

  it('deletes a product with images', async () => {
    const product = {
      name: 'Super Product',
      price: 99.99,
      categoryId: 1
    };

    const productImage = {
      url: 'http://www.server.com/random.jpg',
      productId: 1
    };

    await dbHelper.seedDb({
      products: [product],
      productImages: [productImage]
    });

    const response = await request(server).delete('/api/products/1');

    expect(response.status).toBe(204);
    expect(productDeleted).toHaveBeenCalledTimes(1);
    expect(uploader.productImages.remove).toHaveBeenCalledTimes(1);
  });

  it('returns 404 product id doesn not exist', async () => {
    const response = await request(server).delete('/api/products/404');

    expect(response.status).toBe(404);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": "Product with id: 404 doesn't exist.",
        "message": "Not Found",
        "statusCode": 404,
      }
    `);
  });
});
