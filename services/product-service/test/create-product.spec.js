jest.mock('../publisher');
const server = require('../server');
const request = require('supertest');
const dbHelper = require('./db-test-helper')();
const { productCreated } = require('../publisher');

const productCategories = [
  {
    name: 'A'
  },
  {
    name: 'B'
  },
  {
    name: 'C'
  }
];

describe('POST /api/products', () => {
  beforeAll(async () => {
    await dbHelper.setupDb();
    await dbHelper.resetDb();
    await dbHelper.seedDb({ productCategories });
  });
  beforeEach(async () => {
    await dbHelper.resetDb(['productImages', 'products']);
  });
  afterAll(async () => {
    await dbHelper.dropDb();
  });
  it('creates new product', async () => {
    const product = {
      name: 'Super Product',
      price: 99.99,
      categoryId: 1
    };
    const response = await request(server)
      .post('/api/products')
      .send(product);

    expect(response.status).toBe(200);
    expect(response.body.id).toBeDefined();
    expect(response.body.name).toBe(product.name);
    expect(response.body.price).toBe(product.price);
    expect(response.body.categoryId).toBe(product.categoryId);
    expect(response.body.category.name).toBe(productCategories[0].name);
    expect(productCreated).toHaveBeenCalledTimes(1);
  });

  it('validates name', async () => {
    const product = {
      price: 99.99,
      categoryId: 1
    };
    const response = await request(server)
      .post('/api/products')
      .send(product);

    expect(response.status).toBe(400);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": Array [
          Object {
            "context": Object {
              "key": "name",
              "label": "name",
            },
            "message": "\\"name\\" is required",
            "path": Array [
              "name",
            ],
            "type": "any.required",
          },
        ],
        "message": "{
        \\"price\\": 99.99,
        \\"categoryId\\": 1,
        [41m\\"name\\"[0m[31m [1]: -- missing --[0m
      }
      [31m
      [1] \\"name\\" is required[0m",
        "statusCode": 400,
      }
    `);
  });

  it('validates category', async () => {
    const product = {
      name: 'test',
      price: 99.99,
      categoryId: 1234
    };
    const response = await request(server)
      .post('/api/products')
      .send(product);

    expect(response.status).toBe(400);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": "Validation Error",
        "message": "Bad Request",
        "statusCode": 400,
      }
    `);
  });
});
