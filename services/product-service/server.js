const service = require('@pw/core/service');
const routes = require('./routes');
const config = require('./config');

const app = service(app => {
  app.get('/', (req, res) => res.send(`${config.app.name} 🚀`));
  app.use('/api', routes);
});

module.exports = app;
