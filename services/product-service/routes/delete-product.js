const publisher = require('../publisher');

const db = require('../repositories');
const Joi = require('@hapi/joi');
const path = require('path');
const uploader = require('../uploader');

const schema = Joi.number().required();

module.exports = async function deleteProduct(data) {
  const id = Joi.attempt(data, schema);
  const product = await db.product.getById(id);
  const productImages = await db.productImages.findByProductIds([product.id]);
  await Promise.all([
    db.productImages.deleteByProductId(product.id),
    productImages.map(i =>
      uploader.productImages.remove(`${product.id}/${path.basename(i.url)}`)
    )
  ]);

  await db.product.deleteById(product.id);

  publisher.productDeleted(product);
};
