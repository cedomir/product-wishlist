const { Router } = require('express');
const router = Router();
const asyncHandler = require('express-async-handler');
const asyncBusboy = require('async-busboy');

const createProductImages = require('./create-product-images');
const updateProduct = require('./update-product');
const deleteProduct = require('./delete-product');
const createProduct = require('./create-product');
const getProduct = require('./get-product');
const getProducts = require('./get-products');

router.get(
  '/products',
  asyncHandler(async (req, res) => {
    res.send(await getProducts(req.query));
  })
);

router.get(
  '/products/:id',
  asyncHandler(async (req, res) => {
    res.send(await getProduct(req.params.id));
  })
);

router.post(
  '/products',
  asyncHandler(async (req, res) => {
    res.send(await createProduct(req.body));
  })
);

router.delete(
  '/products/:id',
  asyncHandler(async (req, res) => {
    await deleteProduct(req.params.id);
    res.status(204).send();
  })
);

router.put(
  '/products/:id',
  asyncHandler(async (req, res) => {
    res.send(await updateProduct(req.params.id, req.body));
  })
);

router.post(
  '/products/:id/images',
  asyncHandler(async (req, res) => {
    const { files } = await asyncBusboy(req);
    const productImages = await createProductImages(req.params.id, files);
    res.send(productImages);
  })
);

module.exports = router;
