const db = require('../repositories');
const Joi = require('@hapi/joi');
const presentProducts = require('./products-presenter');

const schema = Joi.object({
  limit: Joi.number()
    .required()
    .max(100),
  offset: Joi.number()
    .required()
    .greater(-1)
}).required();

module.exports = async function getProduct(data) {
  const query = Joi.attempt(data, schema);
  const products = await db.product.find(query);
  return presentProducts(products);
};
