const config = require('./config');

module.exports = {
  client: 'mysql2',
  pool: { min: 0, max: 10, idleTimeoutMillis: 500 },
  connection: {
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.name,
    typeCast: function(field, next) {
      if (field.type == 'NEWDECIMAL') {
        return new Number(field.string());
      }
      return next();
    }
  }
};
