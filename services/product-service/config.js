require('dotenv').config();

module.exports = {
  app: {
    name: process.env.APP_NAME,
    port: process.env.PORT || 3000,
    env: process.env.APP_ENV || 'development'
  },
  db: {
    host: process.env.DB_HOST,
    name: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD
  },
  aws: {
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    },
    s3: {
      endpoint: process.env.AWS_S3_ENDPOINT,
      region: process.env.AWS_REGION
    },
    sqs: {
      queueUrl: process.env.AWS_SQS_QUEUE_URL,
      region: process.env.AWS_REGION
    }
  }
};
