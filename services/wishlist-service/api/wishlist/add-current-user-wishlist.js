const db = require('../../repositories');
const Joi = require('@hapi/joi');
const presentWishlists = require('../wishlist-presenter');

const schema = Joi.object({
  name: Joi.string()
    .min(3)
    .max(15)
    .required()
}).required();

module.exports = async function addCurrentUserWishlist(userId, data) {
  const wishlistData = Joi.attempt(data, schema);
  const wishlist = await db.wishlists.insert({
    ...wishlistData,
    userId
  });

  return (await presentWishlists([wishlist]))[0];
};
