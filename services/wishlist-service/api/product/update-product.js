const productService = require('../../proxies/product-service');

module.exports = async function updateProduct(id, data) {
  return productService.updateProduct(id, data);
};
