const deleteProduct = require('./delete-product');

const addProductImage = require('./add-product-image');

const updateProduct = require('./update-product');

const getProductById = require('./get-product-by-id');

const addProduct = require('./add-product');
const getProducts = require('./get-products');

const { Router } = require('express');
const router = Router();
const asyncHandler = require('express-async-handler');
const auth = require('../../middlewares/jwt-middleware');
const asyncBusboy = require('async-busboy');

router.post(
  `/products`,
  auth,
  asyncHandler(async (req, res) => {
    res.send(await addProduct(req.body));
  })
);

router.get(
  `/products`,
  asyncHandler(async (req, res) => {
    res.send(await getProducts(req.query));
  })
);

router.get(
  `/products/:id`,
  asyncHandler(async (req, res) => {
    res.send(await getProductById(req.params.id));
  })
);

router.put(
  `/products/:id`,
  auth,
  asyncHandler(async (req, res) => {
    res.send(await updateProduct(req.params.id, req.body));
  })
);

router.delete(
  `/products/:id`,
  auth,
  asyncHandler(async (req, res) => {
    res.send(await deleteProduct(req.params.id));
  })
);

router.post(
  '/products/:id/images',
  asyncHandler(async (req, res) => {
    const { files } = await asyncBusboy(req);
    const productImages = await addProductImage(req.params.id, files[0]);
    res.send(productImages);
  })
);

module.exports = router;
