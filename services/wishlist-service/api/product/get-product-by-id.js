const productService = require('../../proxies/product-service');

module.exports = async function getProductById(id) {
  return productService.getProductById(id);
};
