const userSvc = require('../../proxies/user-service');

module.exports = function getUserById(id) {
  return userSvc.getUserById(id);
};
