const axios = require('axios');
const { userSvcHost } = require('../config');

const instance = axios.create({
  baseURL: `${userSvcHost}/api`,
  timeout: 5000
});

module.exports = {
  auth: async data => (await instance.post('/auth', data)).data,
  addUser: async data => (await instance.post('/users', data)).data,
  getUserById: async id => (await instance.get(`/users/${id}`)).data
};
