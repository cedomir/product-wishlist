const nock = require('nock');

beforeAll(async function() {
  jest.setTimeout(30000);
  nock.disableNetConnect();
});

afterEach(function() {
  if (!nock.isDone()) {
    const errMsg = `There are unmet expectations: ${nock.pendingMocks()}`;
    nock.cleanAll();
    throw new Error(errMsg);
  }
});
