const config = require('../config');
const MongoClient = require('mongodb').MongoClient;

let connectPromise;

async function connect() {
  if (connectPromise) {
    return connectPromise;
  }
  connectPromise = MongoClient.connect(config.mongo.url);
  return connectPromise;
}

module.exports = {
  connect,
  getDb: async () => {
    const client = await connect();
    return client.db(config.mongo.database);
  }
};
