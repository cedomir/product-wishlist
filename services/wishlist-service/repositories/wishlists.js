const client = require('./client');
const handleError = require('./db-error-handler');
const Boom = require('@hapi/boom');
const { ObjectId } = require('mongodb');

module.exports = {
  insert: async data => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection.insertOne({
        ...data,
        createdAt: new Date()
      });
      return result.ops[0];
    } catch (err) {
      handleError(err);
    }
  },
  getByUserId: async userId => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection.find({ userId }).toArray();
      return result;
    } catch (err) {
      handleError(err);
    }
  },
  getByProductId: async productId => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection
        .find({
          productIds: { $in: [productId] }
        })
        .toArray();
      return result;
    } catch (err) {
      handleError(err);
    }
  },
  getById: async id => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection.findOne({ _id: new ObjectId(id) });
      if (!result) {
        throw Boom.notFound('Wishlist not found', { id });
      }
      return result;
    } catch (err) {
      handleError(err);
    }
  },
  addProduct: async (wishlistId, productId) => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection.update(
        { _id: new ObjectId(wishlistId) },
        {
          $push: { productIds: productId }
        }
      );
      return result;
    } catch (err) {
      handleError(err);
    }
  },

  delete: async wishlistId => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection.deleteOne({
        _id: new ObjectId(wishlistId)
      });
      return result;
    } catch (err) {
      handleError(err);
    }
  },

  update: async (wishlistId, data) => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection.update(
        {
          _id: new ObjectId(wishlistId)
        },
        { $set: { ...data } }
      );
      return result;
    } catch (err) {
      handleError(err);
    }
  }
};
