const logger = require('@pw/core/logger');
const db = require('./repositories');

module.exports = socketServer => async message => {
  logger.info(`Handle ${message.type}`, { ...message });

  if (
    message.type === 'PRODUCT_UPDATED' ||
    message.type === 'PRODUCT_DELETED'
  ) {
    const affectedWishlists = await db.wishlists.getByProductId(
      message.payload.id
    );
    socketServer.sendForWishlists(affectedWishlists, message);
  }
};
