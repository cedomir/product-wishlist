require('dotenv').config();

module.exports = {
  app: {
    name: process.env.APP_NAME,
    port: process.env.PORT || 3000,
    env: process.env.APP_ENV || 'development'
  },
  userSvcHost: process.env.USER_SVC_HOST,
  productSvcHost: process.env.PRODUCT_SVC_HOST,
  jwt: {
    publicKey: process.env.JWT_PUBLIC_KEY
  },
  mongo: {
    url: process.env.MONGO_URL,
    database: process.env.MONGO_DATABASE
  },
  aws: {
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    },
    sqs: {
      queueUrl: process.env.AWS_SQS_QUEUE_URL,
      region: process.env.AWS_REGION
    }
  }
};
