const db = require('../repositories');
const server = require('../server');
const request = require('supertest');
const Boom = require('@hapi/boom');

jest.mock('../repositories');

describe('GET /api/users', () => {
  it('returns user', async () => {
    const user = {
      username: 'test-user',
      password: 'passw0rd',
      gender: 'M',
      _id: '5d7cb8f0d3b65ad1fbac8998',
      createdAt: new Date().toISOString()
    };

    db.users.getById.mockResolvedValue({
      ...user
    });

    const response = await request(server).get(`/api/users/${user._id}`);

    expect(response.status).toBe(200);
    expect(response.body.username).toBe(user.username);
    expect(response.body.gender).toBe(user.gender);
    expect(response.body._id).toBe(user._id);
    expect(response.body.createdAt).toBe(user.createdAt);
    expect(response.body.password).not.toBeDefined();
    expect(response.body.token).not.toBeDefined();
  });

  it('validates data', async () => {
    const response = await request(server).get(`/api/users/fake-id`);

    expect(response.status).toBe(400);
    expect(response.body.message).toMatchInlineSnapshot(
      `"\\"value\\" with value \\"fake-id\\" fails to match the required pattern: /^[0-9a-fA-F]{24}$/"`
    );
  });

  it("return error if user doesn't exist", async () => {
    db.users.getById.mockRejectedValue(Boom.notFound());

    const response = await request(server).get(
      `/api/users/5d7cb8f0d3b65ad1fbac8998`
    );

    expect(response.status).toBe(404);
    expect(response.body.message).toMatchInlineSnapshot(`"Not Found"`);
  });
});
