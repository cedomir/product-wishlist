const config = require('./config');
const jwt = require('jsonwebtoken');

module.exports = {
  sign: payload => {
    return jwt.sign(
      payload,
      { key: config.jwt.privateKey, passphrase: config.jwt.passphrase },
      { algorithm: 'RS256' }
    );
  },
  verify: token => {
    return jwt.verify(token, config.jwt.publicKey, { algorithms: ['RS256'] });
  }
};
