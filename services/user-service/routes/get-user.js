const db = require('../repositories');
var Joi = require('@hapi/joi');
const joiObjectId = require('joi-objectid')(Joi);
const presentUser = require('./user-presenter');

const idShema = joiObjectId();

module.exports = async function getById(id) {
  const objectId = Joi.attempt(id, idShema);
  const user = await db.users.getById(objectId);
  return presentUser(user);
};
