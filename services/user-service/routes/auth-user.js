const passwordService = require('../password-service');
const jwtService = require('../jwt-service');
const presentUser = require('./user-presenter');

const db = require('../repositories');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const authSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required()
}).required();

module.exports = async function authUser(data) {
  const { username, password } = Joi.attempt(data, authSchema);
  const user = await db.users.getByUsername(username);
  const passwordMatch = await passwordService.compare(password, user.password);

  if (!passwordMatch) {
    throw Boom.notFound('User not found.');
  }

  const token = jwtService.sign({ id: user._id });

  return { user: presentUser(user), token };
};
