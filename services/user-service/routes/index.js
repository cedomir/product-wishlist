const authUser = require('./auth-user');

const createUser = require('./create-user');
const { Router } = require('express');
const router = Router();
const asyncHandler = require('express-async-handler');
const getUser = require('./get-user');
const { verify } = require('../jwt-service');

router.get(
  '/users/:id',
  asyncHandler(async (req, res) => {
    res.send(await getUser(req.params.id));
  })
);

router.post(
  '/users',
  asyncHandler(async (req, res) => {
    res.send(await createUser(req.body));
  })
);

router.post(
  '/auth',
  asyncHandler(async (req, res) => {
    res.send(await authUser(req.body));
  })
);

router.post(
  '/auth/verify',
  asyncHandler(async (req, res) => {
    const { token } = req.body;
    const verified = verify(token);
    res.send(verified);
  })
);

module.exports = router;
