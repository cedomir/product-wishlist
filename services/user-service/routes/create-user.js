const db = require('../repositories');
const Joi = require('@hapi/joi');
const presentUser = require('./user-presenter');
const passwordService = require('../password-service');
const jwtService = require('../jwt-service');

const userDataSchema = Joi.object({
  username: Joi.string()
    .min(5)
    .max(15)
    .required(),
  password: Joi.string()
    .min(5)
    .max(15)
    .required(),
  gender: Joi.string().only(['M', 'W', 'K'])
}).required();

module.exports = async function createUser(data) {
  const userData = Joi.attempt(data, userDataSchema);
  const encryptedPassword = await passwordService.hash(userData.password);
  const user = await db.users.insert({
    ...userData,
    password: encryptedPassword
  });

  const token = jwtService.sign({ id: user._id });

  return { user: presentUser(user), token };
};
