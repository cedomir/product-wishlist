const config = require('./config');
const logger = require('@pw/core/logger');
const server = require('./server');
const dbClient = require('./repositories/client');

dbClient.connect().then(() => {
  logger.info('Connected to DB...');
});

server.listen(config.app.port, () => {
  logger.info(
    `Server ${config.app.name} started on port ${config.app.port} ...`
  );
});
