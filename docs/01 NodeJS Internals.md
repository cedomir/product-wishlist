# NodeJS Internals

http://github.com/nodejs/node

From https://nodejs.org/en/about/

> As an asynchronous event-driven JavaScript runtime, Node.js is designed to build scalable network applications.

From definition two main characteristics are:

- JavaScript runtime provided by `V8 JavaScript Engine`.
- Asynchronous event-driven provided by `libuv`.

## V8 JavaScript Engine

https://github.com/v8/v8

- V8 is Google's open source JavaScript engine.
- V8 implements ECMAScript as specified in ECMA-262.
- V8 is written in C++ and is used in Google Chrome, the open source browser from Google.
- V8 can run standalone, or can be embedded into any C++ application.

## libuv

https://github.com/libuv/libuv

libuv is multi-platform support library with a focus on asynchronous I/O

- Full-featured event loop backed by epoll, kqueue, IOCP, event ports.
- Asynchronous TCP and UDP sockets
- Asynchronous DNS resolution
- Asynchronous file and file system operations
- File system events
- ANSI escape code controlled TTY
- IPC with socket sharing, using Unix domain sockets or named pipes (Windows)
- Child processes
- Thread pool
- Signal handling
- High resolution clock
- Threading and synchronization primitives

![alt text](./nodejs-internals.png 'Node.js Internals')

## Example how crypto.pbkdf2 is implemented

Docs: https://nodejs.org/api/crypto.html#crypto_crypto_pbkdf2_password_salt_iterations_keylen_digest_callback

How we use it:

```js
const crypto = require('crypto');
crypto.pbkdf2('secret', 'salt', 100000, 64, 'sha512', (err, derivedKey) => {
  if (err) throw err;
  console.log(derivedKey.toString('hex')); // '3745e48...08d59ae'
});
```

How node JS module is implemented:
https://github.com/nodejs/node/blob/master/lib/internal/crypto/pbkdf2.js#L19

```js
const { pbkdf2: _pbkdf2 } = internalBinding('crypto');

function pbkdf2(password, salt, iterations, keylen, digest, callback) {
  if (typeof digest === 'function') {
    callback = digest;
    digest = undefined;
  }

  ({ password, salt, iterations, keylen, digest } = check(
    password,
    salt,
    iterations,
    keylen,
    digest
  ));

  if (typeof callback !== 'function') throw new ERR_INVALID_CALLBACK(callback);

  const encoding = getDefaultEncoding();
  const keybuf = Buffer.alloc(keylen);

  const wrap = new AsyncWrap(Providers.PBKDF2REQUEST);
  wrap.ondone = ok => {
    // Retains keybuf while request is in flight.
    if (!ok) return callback.call(wrap, new ERR_CRYPTO_PBKDF2_ERROR());
    if (encoding === 'buffer') return callback.call(wrap, null, keybuf);
    callback.call(wrap, null, keybuf.toString(encoding));
  };

  handleError(
    _pbkdf2(keybuf, password, salt, iterations, digest, wrap), // Calls C++ function
    digest
  );
}
```

How node C++ lib is implemented:
https://github.com/nodejs/node/blob/master/src/node_crypto.cc#L6205

```C++

using v8::FunctionCallbackInfo;
//..
inline void PBKDF2(const FunctionCallbackInfo<Value>& args) {
  auto rv = args.GetReturnValue();
  //..
  CHECK(args[4]->IsString());  // digest_name
  //...
  std::unique_ptr<PBKDF2Job> job(new PBKDF2Job(env));
  //...
  env->PrintSyncTrace();
  job->DoThreadPoolWork(); // Do work on a thread
  rv.Set(job->ToResult());
}
//..
 env->SetMethod(target, "pbkdf2", PBKDF2);
```

How many threads does Node.js process have?

```
UV_THREADPOOL_SIZE + v8_thread_pool_size + 1
```

`UV_THREADPOOL_SIZE` default is 4 max 128
`v8_thread_pool_size` default is 4

From: https://nodejs.org/api/cli.html#cli_uv_threadpool_size_size - must read!

> Asynchronous system APIs are used by Node.js whenever possible, but where they do not exist, libuv's threadpool is used to create asynchronous node APIs based on synchronous system APIs. Node.js APIs that use the threadpool are:
>
> - all fs APIs, other than the file watcher APIs and those that are explicitly synchronous
>   asynchronous crypto APIs such as crypto.pbkdf2(), crypto.scrypt(), crypto.randomBytes(), crypto.randomFill(), crypto.generateKeyPair()
> - dns.lookup()
> - all zlib APIs, other than those that are explicitly synchronous

**By default DNS lookups are done on uv threadpool because underlying getaddrinfo function is sync. If dns takes too much time to respond, thread pool will soon be blocked.** To fix this issue do not just increase UV_THREADPOOL_SIZE size - your app will just be slow, instead make sure that you are caching dns lookups. If you can't rely on infrasturcture to do dns caching, you can use https://github.com/eduardbcom/lookup-dns-cache to do dns caching on application side.

#### What is the difference in execution?

In `demos/thread-pool-demo.js`, you can run examples as:

```bash
$ node demos/thread-pool-demo.js example1
```

```js
const crypto = require('crypto');
const start = Date.now();

function logHashTime() {
  crypto.pbkdf2('a', 'b', 100000, 512, 'sha512', () => {
    console.log('Hash: ', Date.now() - start);
  });
}

function example1() {
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
}

function example2() {
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
}

function example3() {
  process.env.UV_THREADPOOL_SIZE = 5;

  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
}
```

## What is Event Loop?

Note: This is very simplified, high level view of how node runs your programs.

In it's main thread node is executing your code using V8. When function is called it is put on a stack, when function execution ends it is removed from the stack.

When calling async function, node executes it on another thread so that V8 can continue runing your code. When operation finishes, callback is put into a FIFO `Task Queue` to be executed. Callbacks are taken one by one from task queue and executed in sequence.

Node provides two different queues for executing callbacks: microtasks and macrotasks.

examples of microtasks:

- process.nextTick
- promises
- Object.observe

examples of macrotasks:

- setTimeout
- setInterval
- setImmediate
- I/O

![alt text](./nodejs-event-loop.png 'Node.js Event Loop')

What is the output?

In `demos/event-loop-demo.js`, you can run examples as:

```bash
$ node demos/event-loop-demo.js example1
```

```js
function example1() {
  console.log('start');
  setTimeout(() => console.log('setTimeout'), 0);
  setImmediate(() => console.log('setImmediate'));
  Promise.resolve().then(() => console.log('Promise'));
  process.nextTick(() => console.log('process.nextTick'));
  console.log('end');
}
```

```js
function example2() {
  console.log('start');
  setTimeout(() => {
    console.log('setTimeout');
    Promise.resolve().then(() => console.log('Promise'));
    setTimeout(() => {
      console.log('setTimeout');
      process.nextTick(() => console.log('process.nextTick'));
      setTimeout(() => console.log('setTimeout'), 0);
    }, 0);
  }, 0);
  setImmediate(() => console.log('setImmediate'));
  console.log('end');
}
```

## Good Reads

- https://medium.com/better-programming/learn-node-js-under-the-hood-37966a20e127
- https://medium.com/better-programming/is-node-js-really-single-threaded-7ea59bcc8d64
- https://blog.risingstack.com/node-js-at-scale-understanding-node-js-event-loop/
- https://www.youtube.com/watch?v=8aGhZQkoFbQ
