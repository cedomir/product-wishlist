# Working with MySQL

- Explain different alternatives

  - Setup `MySQL` in `docker-compose.yaml`
  - Setup migrations

## MySQL Database Library Alternatives

### KnexJS

A query builder for PostgreSQL, MySQL and SQLite3, designed to be flexible, portable, and fun to use. http://knexjs.org (10 468 ⭐️)

### Sequalize

An easy-to-use multi SQL dialect ORM for Node.js https://sequelize.org (19 839 ⭐️)

### TypeORM

ORM for TypeScript and JavaScript (ES7, ES6, ES5). Supports MySQL, PostgreSQL, MariaDB, SQLite, MS SQL Server, Oracle, WebSQL databases. Works in NodeJS, Browser, Ionic, Cordova and Electron platforms. http://typeorm.io (15 021 ⭐️)

## Knex Setup

- Add MySQL server to `docker-compose.yml`
- Add db connection details to `.env` and `config.js`
- Add `mysql2`
- Add `knex` for db migrations and query building (http://knexjs.org/).
- Add `knexfile.js` to configure db connection for migrations cli (http://knexjs.org/#Migrations).
  - TIP: Use config to setup db connection details
- Add tasks in `package.json` for easier migration management
  - `migrate:latest` to run migrations
  - `migrate:down` to rollback last migration
  - `migrate:make` to generate new migration
  - TIP: you can also use `npx knex migrate:make` for example
- Create migration for following tables
  - productCategories:
    - id: auto increment
    - name: string, required
  - products:
    - id: auto increment
    - name: string, required
    - price: decimal, required
    - categoryId: int, foreign key `productCategories.id`

## Store products in DB

- Build product repository
- Connect api to db, check all business rules
- Handle db errors

## Create `product-presenter` to prepare data from api response

```json
{
  "id": 1,
  "name": "Super Shoes",
  "price": 99.99,
  "category": {
    "id": 1,
    "name": "Shoes"
  }
}
```

## Practice

Connect following endpoints with db repositories

- `GET /api/products/:id`
- `PUT /api/products/:id`
- `DELETE /api/products/:id`
- `GET /api/products?limit=:limit&offset=:offset`
