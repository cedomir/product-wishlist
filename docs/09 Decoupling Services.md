# Decoupling Services

- Separation of product service
  - Move all product related code to new service folder
  - Run product service as separate process
  - Use axios to connect to product service from wishlist
  - Writing tests by mocking network calls using `nock`
- Explain `yarn workspaces`
  - Configure product service as a separate module

## Yarn Workspaces

## Install yarn

We are going to utilize `yarn workspaces` feature to separate modules in our monorepo and to manage dependencies.

You can find install instructions for your OS here: https://yarnpkg.com/lang/en/docs/install

For macOS use brew:

```
$ brew install yarn
```

## Configure root package.json

In project root create package.json with following contents

```
{
  "private": true,
  "workspaces": ["services/*"]
}
```

`private: true` is required for workspaces to work since workspace is not meant to be published. `workspaces` is a list of paths containing our modules.

# Setup product service

- Create `core` module to store all common code
  - server.js, config.js, logger.js, error-handler.js...
- Create `services/product-service` folder
- Create `.env` file
- Copy product routes, db repositories, migrations
- Update every service `package.json` to expose `start`, `start:dev` and test tasks
- Use `concurently` to start multiple processes

## Write `product-service` proxy

- Write proxy using axios http client
- Make sure to handle all errors
- Update tests to mock calls to `product-service` using `nock`

## Practice

- Separate user service from wishlist, configure as separate module
