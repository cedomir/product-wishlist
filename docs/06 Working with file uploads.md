# Working with file uploads

## Create migration for product images

- productImages:

  - id: auto increment
  - url: string, required
  - productId: int, foreign key `product.id`

# Setup S3 bucket

Take a look at `scripts/localstack/init.sh` how s3 bucket is setup:

```bash
export S3_BUCKET=pw-product-images
export AWS_REGION=us-east-1

$ awslocal s3api create-bucket --bucket $S3_BUCKET --region $AWS_REGION
$ awslocal s3api put-bucket-acl --bucket $S3_BUCKET --acl public-read --region $AWS_REGION
```

## Handling file upload

- Create route `POST /products/:id/images`
- Check if product exists
- Upload image to S3
  - TIP: use `aws-sdk` and `async-busboy`
- Save to DB

## Change product-presenter to return images

```json
{
  "id": 1,
  "name": "Super Shoes",
  "price": 99.99,
  "category": {
    "id": 1,
    "name": "Shoes"
  },
  "images": [
    {
      "id": 1,
      "url": "https://product-images.com/1.jpg"
    }
  ]
}
```
