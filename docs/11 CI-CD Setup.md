# CI-CD Setup

Goal is to test all services and build docker images using Gitlab CI and to deploy to Heroku.

We want test to run on every commit and only deploy if we merge to master.

For basic setup, take a look at https://gitlab.com/amirilovic/gitlab2heroku

- Writing `Dockerfile`
- Writing `.gitlab-ci.yml`
- Configuring `heroku` applications
- Configuring `GitLab` repository
