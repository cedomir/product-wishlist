#!/bin/bash

export S3_BUCKET=pw-product-images
export SQS_QUEUE=pw-products
export AWS_REGION=us-east-1

awslocal sqs create-queue --queue-name $SQS_QUEUE --region $AWS_REGION

awslocal s3api create-bucket --bucket $S3_BUCKET --region $AWS_REGION
awslocal s3api put-bucket-acl --bucket $S3_BUCKET --acl public-read --region $AWS_REGION

cat /dev/null >/tmp/localstack_infra.log
