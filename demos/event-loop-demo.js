function example1() {
  console.log('start');
  setTimeout(() => console.log('setTimeout'), 0);
  setImmediate(() => console.log('setImmediate'));
  Promise.resolve().then(() => console.log('Promise'));
  process.nextTick(() => console.log('process.nextTick'));
  console.log('end');
}

function example2() {
  console.log('start');
  setTimeout(() => {
    console.log('setTimeout');
    Promise.resolve().then(() => console.log('Promise'));
    setTimeout(() => {
      console.log('setTimeout');
      process.nextTick(() => console.log('process.nextTick'));
      setTimeout(() => console.log('setTimeout'), 0);
    }, 0);
  }, 0);
  setImmediate(() => console.log('setImmediate'));
  console.log('end');
}

const examples = { example1, example2 };

const fn = examples[process.argv[2]];

if (typeof fn == 'undefined') {
  console.error(`Example not found. Run script like:
    $ node event-loop-demo.js example1
    `);
  process.exit(1);
}

setTimeout(fn, 0);
