const crypto = require('crypto');
const start = Date.now();

function logHashTime() {
  crypto.pbkdf2('a', 'b', 100000, 512, 'sha512', () => {
    console.log('Hash: ', Date.now() - start);
  });
}

function example1() {
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
}

function example2() {
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
}

function example3() {
  process.env.UV_THREADPOOL_SIZE = 5;

  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
  logHashTime();
}

const examples = { example1, example2, example3 };

const fn = examples[process.argv[2]];

if (typeof fn == 'undefined') {
  console.error(`Example not found. Run script like:
    $ node thread-pool-demo.js example1
    `);
  process.exit(1);
}

fn();
